# Études in C  
a study in C programming  

[//]: # (Image References)
[image1]: ./images/Ritchie_and_Thompson.jpg
[image2]: ./images/Torvalds.jpg


## Why program in C?  
The reasons are legion, but here are a just a few compelling ones:  

![alt text][image1]  
and  
![alt text][image2]  

and finally, this excerpt from the preface of Ben Klemens' book "21st Century C"  (used with the author's permission):

"**C Is Punk Rock**   
*C has only a handful of keywords and is a bit rough around the edges, and it rocks. You can do anything with it. Like the C, G, and D chords on a guitar, you can learn the basic mechanics quickly, and then spend the rest of your life getting better. The people who don't get it fear its power and think it too edgy to be safe. By all rankings, it is consistently the most popular language that doesn't have a corporation or foundation spending money to promote it."*

**Now that you're convinced, let's program in C!**  

### Les Études  

##### [No. 0 - Ring Buffer](./les_etudes/No_000)

##### [No. 1 - XOR decryption](./les_etudes/No_001)

##### [No. 2 - Sort then search for value](./les_etudes/No_002)  

##### [No. 3 - A linked list of structs containing a linked list](./les_etudes/No_003)  

##### [No. 4 - A shared library implementation of a POSIX shared memory object](./les_etudes/No_004)  

##### [No. 5 - Calculating the greatest prime factor](./les_etudes/No_005)  

##### [No. 6 - Implementing a FIFO array](./les_etudes/No_006)  

##### [No. 7 - Identifying neighbors of an element in a 2d array](./les_etudes/No_007)  

##### [No. 8 - Summing numbers too large to be represented by the largest, unsigned data type](./les_etudes/No_008)  

##### [No. 9 - Assorted ideas and techniques a programmer should learn](./les_etudes/No_009)  

##### [No. 10 - Basic Linux systems programming examples](./les_etudes/No_010)  

##### [No. 11 - Struct with union member](./les_etudes/No_011/struct_with_union.c)  

##### [No. 12 - A flexible, void buffer](./les_etudes/No_012/flex_void_buffer.c) 

##### [No. 13 - Growing a dynamic array of struct](./les_etudes/No_013/array_of_player_structs.c) 

##### [No. 14 - An array of struct with a pointer to array member](./les_etudes/No_014/array_of_struct.c)  

##### [No. 15 - An N-dim array as as 1-dim array of cell structs which contain row, col indexes](./les_etudes/No_015/main.c)    

##### [No. 16 - A linked list for multiple data types ](./les_etudes/No_016/list_t)  

##### [No. 17 - A stack for multiple data types ](./les_etudes/No_017/stack_t)  

##### [No. 18 - A queue for multiple data types ](./les_etudes/No_018/queue_t)  

##### [No. 19 - A vector container for multiple data types ](./les_etudes/No_019/vector_t)  

##### [No. 20 - Google Billboard Challenge ](./les_etudes/No_020/readme.md)  
