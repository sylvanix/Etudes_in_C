#include <stdlib.h>
#include <string.h>
#include <stdio.h>

typedef struct {
    char name[30];
    unsigned int ac;
    float age;
} Player;

int growArray(Player **player_array, int currentSize, int numNewElems);


int main()
{
    int size = 0;
    Player* player_array = (Player *) calloc(1, sizeof(Player));
    if(player_array == NULL) {
        printf("Cannot allocate initial memory for data\n");
        exit(1);
    }
    else
        size += 1;

    unsigned int ii;
    ii = 0;
    strcpy(player_array[ii].name, "gabbyGabby");
    printf("player_array[%u]: %s\n", ii, player_array[ii].name);

    size = growArray(&player_array, size, 1); 
    ii += 1;
    strcpy(player_array[ii].name, "welcome_Baku");
    player_array[ii].age = 12.34;
    player_array[ii].ac = 27;
    printf("player_array[%u]: %s\n", ii, player_array[ii].name);

    size = growArray(&player_array, size, 1);
    ii += 1;
    strcpy(player_array[ii].name, "steelyNeil");
    player_array[ii].age = 23.45;
    player_array[ii].ac = 28;
    printf("player_array[%u]: %s\n", ii, player_array[ii].name);

    size = growArray(&player_array, size, 1);
    ii += 1;
    strcpy(player_array[ii].name, "yetAnotherYack");
    player_array[ii].age = 34.56;
    player_array[ii].ac = 29;
    printf("player_array[%u]: %s\n", ii, player_array[ii].name);

    size = growArray(&player_array, size, 1);
    ii += 1;
    strcpy(player_array[ii].name, "defTon3");
    player_array[ii].age = 45.67;
    player_array[ii].ac = 30;
    printf("player_array[%u]: %s\n", ii, player_array[ii].name);
    
    free(player_array);
}


int growArray(Player **player_array, int currentSize, int numNewElems) {
    const int totalSize = currentSize + numNewElems;
    Player *temp = (Player*)realloc(*player_array, (totalSize * sizeof(Player)));

    if (temp == NULL){
        printf("Cannot allocate more memory.\n");
        return 0;
    }
    else
        *player_array = temp;

    return totalSize;
}
