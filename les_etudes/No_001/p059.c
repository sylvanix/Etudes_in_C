/* Project Euler - Problem 59 - XOR decryption */

// To compile with gcc:
// gcc p059.c -o p059 -g -Wall -O3 -std=gnu11 -lm

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>    // pow()

typedef struct {
  int *array;
  size_t used;
  size_t size;
} Array;

void initArray(Array *a, size_t initialSize);
void insertArray(Array *a, int element);
void freeArray(Array *a);
int intXOR(int x, int y);
void printSeqRecur(int num, int pos, int k, int n);


/* read all lines from the input file */
int main() {

    char const filename[] = "./p059_cipher.txt";
    char const delimit[] = ",";
    FILE *pf = NULL;

    if ( ( pf = fopen ( filename, "r")) == NULL) {
        printf ( "could not open file\n");
        }

    char line[3600];
    Array a;

    initArray(&a, 3);  // initially 3 elements
    while ( ( fgets ( line, sizeof (line), pf))) {
        // split on the delimiter
        char *p = strtok (line, delimit);
        int ii = 0;
        while (p != NULL) {
            //printf("%s ", p);
            insertArray(&a, atoi(p));
            p = strtok (NULL, delimit);
            ii++;
        }
    }


    // Generate all length n combinations of k letters. If any key XORed
    // with the cipher text produces English words, then goto KEY_FOUND
    // label. If no key yields English words, then goto NOT_FOUND.
    int n = 3;      // key contains n characters
    int kmin = 97;  // ascii 'a'
    int kmax = 122; // ascii 'z'
    int k = kmax - kmin; // k=26
    int row, col;
    int cell;
    int rdiv;
    int nbr_comb = pow(k+1, n);
    int guess[n];
    int xor1, xor2, xor3;
    int txtIntBuf[3600];
    char *s1;
    char *s2;
    int foundCount = 0;

    for (row=0; row < nbr_comb; row++)
    {
        for (col=n-1; col>=0; col--)
        {
            rdiv = pow(k+1, col);
            cell = kmin + (row/rdiv) % (k+1);
            guess[col] = cell;
        }

        printf("Trying key: %c%c%c\n", guess[0], guess[1], guess[2]);
        // XOR the encrypted text with a key of the same length. The key is
        // a repeated sequence of the three-letters in the array "guess".
        for (int i=0; i<a.used+n; i++) {
            if (i>0 && i%3==0) {
                xor1 = intXOR(a.array[i-3], guess[0]);
                xor2 = intXOR(a.array[i-2], guess[1]);
                xor3 = intXOR(a.array[i-1], guess[2]);
                txtIntBuf[i-3] = xor1;
                txtIntBuf[i-2] = xor2;
                txtIntBuf[i-1] = xor3;
            }

        }
        char strBuf[3600];
        // convert integer array to string
        for (int ib=0; ib<a.used+3; ib++)
            strBuf[ib] = txtIntBuf[ib];

        // Check if some common English words (include surrounding spaces) are
        // in the text. If so, then it likely contains additional English words.
        s1 = strstr(strBuf, " and ");
        s2 = strstr(strBuf, " the ");
        if ( (s1 != NULL) || (s2 != NULL) ) {
            foundCount++;
            for (int ib=0; ib<a.used; ib++)
                printf("%c", strBuf[ib]);
            goto KEY_FOUND;
        }

    }
    if (foundCount==0)
        goto NOT_FOUND;

    KEY_FOUND:
        printf("\n\nDecryption key Found: ");
        printf("%c%c%c\n\n", guess[0], guess[1], guess[2]);
        goto FREE_MEM;

    NOT_FOUND:
        printf("\n\nNo key was found to decrypt the cipher text.");

    FREE_MEM:
        freeArray(&a);
}


void initArray(Array *a, size_t initialSize) {
  a->array = (int *)malloc(initialSize * sizeof(int));
  a->used = 0;
  a->size = initialSize;
}

void insertArray(Array *a, int element) {
  if (a->used == a->size) {
    a->size *= 2;
    a->array = (int *)realloc(a->array, a->size * sizeof(int));
  }
  a->array[a->used++] = element;
}

void freeArray(Array *a) {
  free(a->array);
  a->array = NULL;
  a->used = a->size = 0;
}

// Returns XOR of integers x and y
int intXOR(int x, int y) {
   return (x | y) & (~x | ~y);
}

/* Generates all sequences of length k */
void printSeqRecur(int num, int pos, int k, int n)
{
    if (pos == k) {
        printf("%d \n", num);
        return;
    }
    for (int i = 1; i <= n; i++) {
        printSeqRecur(num * 10 + i, pos + 1, k, n);
    }
}
