/*
   An example of finding each array element's neighbors. Elements along a border will have fewer
   neighbors than the other elements, which have 8 neighbors.
   ~$ gcc neighbors.c -o neighb -g -Wall -O3 -std=gnu11
*/

#include <stdio.h>

#define MIN(a,b) (((a)<(b))?(a):(b))
#define MAX(a,b) (((a)>(b))?(a):(b))

int main()
{
    const unsigned int ROWS = 5;
    const unsigned int COLS = 8;
    unsigned int arr[ROWS][COLS];

    // initialize the 2d array
    unsigned int count = 0;
    for(unsigned int irow=0; irow<ROWS; irow++ ) {
        for(unsigned int icol=0; icol<COLS; icol++) {
            //printf("row, col: %u %u   %u\n", irow, icol, count);
            arr[irow][icol] = count;
            count += 1;
        }
        //printf("\n");
    }

    // print the 2d array for checking neighbor-finding results below
    printf("\nThe 2D array for which we will find each element's neighbors:\n");
    for(unsigned int irow=0; irow<ROWS; irow++ ) {
        for(unsigned int icol=0; icol<COLS; icol++) {
            printf(" %2u", arr[irow][icol]);
        }
        printf("\n");
    }
    printf("\n");


    // find each elements neighbours
    printf("Below are printed each element followed by its neighbors:\n");
    for(int i=0; i<ROWS; i++) {
        for(int j=0; j<COLS; j++) {
            printf("    %3u: ", arr[i][j]);
            for(int x=MAX(0,i-1); x<=MIN(i+1, ((int)ROWS-1)); x++) {
                for(int y = MAX(0, j-1); y <= MIN(j+1, ((int)COLS-1)); y++) {
                    if(x != i || y != j) {
                        printf(" %2u", arr[x][y]);
                    }  
                }
            }
            printf("\n");       
        }
        printf("\n");
    }


}
