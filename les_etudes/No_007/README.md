## Identifying all neighbors of each element in a 2d array.  
In neighbors.c, a 2d array is instantiated with unsigned integers increasing by one across the columns. Then for each element in this array, all immediate neighbors are identified. Boundary checking is performed so that when identifying neighbors of elements on the borders, no check is made outside of the array.  

### Instructions  

Compile and run:   
```
~$ gcc neighbors.c -o neighb -g -Wall -O3 -std=gnu11
~$ ./neighb
```

#### Sources  

1. The answer by Seb to the question "Is there an easy way of finding the neighbours of an element in a two-dimensional array?":  
https://stackoverflow.com/questions/652106/finding-neighbours-in-a-two-dimensional-array  
