#include<stdio.h>
#include<stdlib.h> // malloc
#include<string.h> // memcpy()


typedef struct item_
{
    char name[30];
    size_t count;
    double value;   // in gold
} item;


typedef struct PlayerCharacter
{
    char name[30];
    double age;      // years
    double height;   // centimeters
    size_t nb_items;
    item *inventory; // a pointer to an array of items
} plchar;


int main()
{
    size_t N = 3; // number of player characters
    plchar *players = malloc(sizeof(plchar)* N); // a dynamic array of player characters

    // create a player at index 0
    char *tempname0 = "abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz";
    memcpy(players[0].name, tempname0, 30);
    players[0].age = 26.73;
    players[0].height = 184.50;
    players[0].nb_items = 0;

    // create an inventory for the player
    size_t initial_nb_items = 5;
    item *items = malloc(sizeof(item) * initial_nb_items);

    // add an item to the inventory
    char *tempItemName0 = "crusty bread";
    memcpy(items[0].name, tempItemName0, 30);
    items[0].count = 1;
    items[0].value = 0.05;
    players[0].nb_items += 1;

    // add another item to the inventory
    char *tempItemName1 = "rusty sword";
    memcpy(items[1].name, tempItemName1, 30);
    items[1].count = 1;
    items[1].value = 0.70;
    players[0].nb_items += 1;
    
    // set the player's inventory pointer to point at this items array
    players[0].inventory = items;

    printf("%s\n",players[0].name);
    unsigned int nb_items = (unsigned int) players[0].nb_items;
    printf("number of items in player 0 inventory: %u\n", nb_items);
    for (unsigned int i=0; i<nb_items; i++) {
        printf("%s\n",players[0].inventory[i].name);
    }

    return 0;
}

