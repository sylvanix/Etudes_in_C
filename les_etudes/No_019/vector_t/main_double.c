#include <stdio.h>
//#include <stdlib.h>
#include "vector.h"

int main(void)
{
    int initLength = 10;
  
    vector_t v;
    vector_new(&v, sizeof(double), NULL);
    printf("allocatedLength: %d\n", v.allocatedLength);
  
    for (int i = 0; i < initLength; i++) {
        vector_add(&v, &v);
        printf("allocatedLength: %d\n", v.allocatedLength);
    }
  
    double value = 1000.12345;
    // prepend value
    vector_insert_at(&v, 0, &value);
  
    // insert in the middle
    value = 500.67891;
    vector_insert_at(&v, 5, &value);
  
    // same as add... (push_back)
    value = 0.12345;
    vector_insert_at(&v, v.logicalLength, &value);
  
    vector_item_at(&v, 0, &value);
    printf("value at 0: %f\n", value);
  
    vector_item_at(&v, 5, &value);
    printf("value at 5: %f\n", value);
  
    vector_item_at(&v, v.logicalLength - 1, &value);
    printf("value at len-1: %f\n", value);

    vector_destroy(&v);

}
