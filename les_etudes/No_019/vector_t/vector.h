#ifndef __VECTOR_H
#define __VECTOR_H

#include "common.h"

typedef struct {
    void *elements;
    int elementSize;
    int allocatedLength;
    int logicalLength;    // "used" length
    freeFunction freeFn;  // caller may specify own free function
} vector_t;

void vector_new(vector_t *vector, int elementSize, freeFunction freeFn);

void vector_destroy(vector_t *vector);

int vector_size(vector_t *vector);

void vector_add(vector_t *vector, void *element);

void vector_item_at(vector_t *vector, int index, void *target);

void vector_insert_at(vector_t *vector, int index, void *target);

void vector_remove_at(vector_t *vector, int index);

#endif
