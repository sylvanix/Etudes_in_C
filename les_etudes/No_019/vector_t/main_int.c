#include <stdio.h>
//#include <stdlib.h>
#include "vector.h"

int main(void)
{
    int initLength = 10;
  
    vector_t v;
    vector_new(&v, sizeof(int), NULL);
  
    int i;
    for (i = 0; i < initLength; i++) {
      vector_add(&v, &v);
    }
  
    int value = 1000;
    // prepend value
    vector_insert_at(&v, 0, &value);
  
    // insert in the middle
    value = 500;
    vector_insert_at(&v, 5, &value);
  
    // same as add...
    value = 0;
    vector_insert_at(&v, v.logicalLength, &value);
  
    vector_item_at(&v, 0, &value);
    printf("value at 0: %d\n", value);
  
    vector_item_at(&v, 5, &value);
    printf("value at 5: %d\n", value);
  
    vector_item_at(&v, v.logicalLength - 1, &value);
    printf("value at len-1: %d\n", value);
  
    vector_destroy(&v);

}
