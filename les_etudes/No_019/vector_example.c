#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "vector_t/vector.h"


// the datatype to be contained in a vector
typedef struct mydtype_ {
    unsigned int id;
    double cost;
    const char* name;
} mytype_t;


// function declarations
int comp_id (const void * elem1, const void * elem2);
int comp_cost (const void * elem1, const void * elem2);


int main(void)
{
    // seed the random number generator
    srand(time(NULL));

    // instantiate a vector
    vector_t v;
    vector_new(&v, sizeof(mytype_t), NULL);

    mytype_t mydata;
    unsigned int i;

    // append elements to the vector
    for(i=0; i<1000; i++) {
        mydata.id = rand()%1000;
        if (i==0) mydata.cost = 0.00;
        else mydata.cost += 0.01;
        mydata.name = "test";
        vector_insert_at(&v, i, &mydata);
    }

    // sort the vector
    qsort (v.elements, v.logicalLength, v.elementSize, comp_cost);
    
    // print vector
    for (i=0; i<v.logicalLength; i++) {
        vector_item_at(&v, i, &mydata);
        printf("%d: (%d, %f, %s)\n", i, mydata.id, mydata.cost, mydata.name);
    }

    // free vector memory
    vector_destroy(&v);
}


/* Function Definitions */

// comparator function for qsort - sort by struct member "id"
int comp_id (const void * elem1, const void * elem2) {
    mytype_t f = *((mytype_t*)elem1);
    mytype_t s = *((mytype_t*)elem2);
    if (f.id > s.id) return  1;
    if (f.id < s.id) return -1;
    return 0;
}

// comparator function for qsort - sort by struct member "cost"
int comp_cost (const void * elem1, const void * elem2) {
    mytype_t f = *((mytype_t*)elem1);
    mytype_t s = *((mytype_t*)elem2);
    if (f.cost > s.cost) return  1;
    if (f.cost < s.cost) return -1;
    return 0;
}

