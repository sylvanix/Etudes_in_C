#ifndef WORLD_H_
#define WORLD_H_

#define ROWS 5
#define COLS 8
#define DEFAULT_TEMPERATURE 20.0
#define DEFAULT_WORLD {"Aarda", 1396, ROWS, COLS, malloc(sizeof(world)* ROWS * COLS)}

typedef struct cell_
{
    size_t col;
    size_t row;
    size_t index;
    double temperature;
} cell;


typedef struct world_
{
    char name[30];
    double year;    // years
    unsigned int nrows;
    unsigned int ncols;
    cell *worldmap; // a pointer to an array of cells
} world;



// function declarations ////////////////////////////////
void initialize_cell_values(world *theWorld);
void printWorldMap(world *theWorld);
void printWorldTemperatures(world *theWorld);


#endif // WORLD_H_
