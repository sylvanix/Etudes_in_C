/*
The world struct contains a pointer to an array of cell structs, which
we may call a "map". This array is 1-dim but has the length of ROWS*COLS,
that is, all elements of a 2-dim array. Each cell in the array contains
the 2-dim column and row indexes, as well as the 1-dim index.
*/

//#include<stdio.h>
#include<stdlib.h> // malloc
#include "world.h"
#include "player.h"


int main()
{
    // initialize the world
    world ourWorld = DEFAULT_WORLD;
    initialize_cell_values(&ourWorld);

    printWorldMap(&ourWorld);
    //printWorldTemperatures(&ourWorld);

    // initialize the player
    player thePlayer;
    initialize_player(&thePlayer);

    item anItem;
    anItem = init_crusty_bread();

    //next, add the item to the player inventory (list). Then print inventory.

    return 0;
}
