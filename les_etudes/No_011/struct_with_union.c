/*
    A union can be thought of as a slightly-less-kludgy way of casting a pointer.
    The structs below, which may be nodes in a list, can contain values of either
    type int or float.
*/

#include <stdio.h>

typedef enum {
    N_INT,
    N_FLOAT
} NumType;

union number {
    int i;
    float f;
};

struct node {
    NumType type;
    union number data;
    struct node *next;
};

typedef struct node item;

int main(void)
{
    item item_1 = {.type = N_INT};
    item_1.data.i = 23; /* Now contains an int */
    printf("item_1.data is %d\n", item_1.data.i);

    item item_2 = {.type = N_FLOAT};
    item_2.data.f = 23.2323; /* Now contains a float */
    printf("item_2.data is %f\n", item_2.data.f);

  return 0;
}
