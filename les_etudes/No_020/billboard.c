/* 
   Find all 10-digit prime numbers in the first 10,000 digits of Euler's number.
*/

// To compile with gcc:
// gcc billboard.c -o billboard -g -Wall -O3 -std=gnu11 -lm


#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>  // isalpha
#include <math.h>    // sqrt

#define MAX_LINE_LENGTH 80
#define MAX_DIGITS 10001

void get_digits(char *path, char *digits);
int is_prime(unsigned long int x);


int main(int argc, char ** argv)
{
    char digits[MAX_DIGITS];
    char *path = "./first_1e4_digits_of_e.txt";
    get_digits(path, digits);
    unsigned int idig;
    char ten[11];
    char *ptr;
    unsigned long int test_number;
    unsigned int pcount = 1;
    for (idig=0; idig<MAX_DIGITS-10; idig++) {
        //printf("%c", digits[idig]);
        for (unsigned int i=0; i<10; i++) {
            //printf("%c", digits[idig+i]);
            ten[i] = digits[idig+i];
        }
        //for (int j=0; j<10; j++) {
        //    printf("%c", ten[j]);
        //}
        ten[10] = '\n'; // add the terminating null character to the string
        //printf("\n");

        // Skip if first character of string is 0 since no number starts with 0
        if (ten[0] != '0') {
            test_number = strtoul(ten, &ptr, 10);
            //printf("    %lu\n", test_number);
            if (is_prime(test_number)) {
                printf("%u - %lu is prime. - starting digit: %u\n", pcount, test_number, idig);
                pcount += 1;
            }
        }
    }

    return 0;
}

void get_digits(char *path, char *digits) {
    printf("path: %s\n", path);
    char line[MAX_LINE_LENGTH] = {0};
    
    // Open file
    FILE *file = fopen(path, "r");
    if (!file) {
        perror(path);
        //return EXIT_FAILURE;
    }
    
    // Get each line until there are none left
    unsigned int idig = 0;
    while (fgets(line, MAX_LINE_LENGTH, file)) {
        // insert each character into digits array
        unsigned int i = 0;
        while (line[i] != '\0') {
            if (isdigit(line[i]) != 0) {
                //printf("%c", line[i]);
                digits[idig] = line[i];
                idig += 1;
            }
            i += 1;
        }
    }
    // Close file
    if (fclose(file)) {
        perror(path);
    }
}


int is_prime(unsigned long int N) {

    if ( (N==2)||(N==3)||(N==5)||(N==7) ) {
        return 1;
    }

    if ((N % 2 == 0) || (N < 2)) {
        return 0;
    }

    long int sqroot = sqrt(N);
    if( sqroot%2 == 0 ){
        sqroot = sqroot-1;
    }

    int isprime = 0;
    long int j = sqroot;

    while(N % j != 0) {
        j -= 2;
        if(j == 1) isprime = 1;
    }
    return isprime;
}

