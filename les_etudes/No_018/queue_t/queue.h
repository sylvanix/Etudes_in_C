#ifndef __QUEUE_H
#define __QUEUE_H

#include "common.h"
#include "list.h"

typedef struct {
  list_t *list;
} queue_t;

void queue_new(queue_t *q, int elementSize, freeFunction freeFn);

void queue_destroy(queue_t *q);

void queue_enqueue(queue_t *q, void *element);

void queue_dequeue(queue_t *q);

int queue_size(queue_t *q);


#endif
