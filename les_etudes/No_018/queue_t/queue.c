#include <stdlib.h>
#include "queue.h"


void queue_new(queue_t *q, int elementSize, freeFunction freeFn) {
    q->list = malloc(sizeof(list_t));
    list_new(q->list, elementSize, freeFn);
}


void queue_destroy(queue_t *q) {
    list_free(q->list);
    free(q->list);
}


void queue_enqueue(queue_t *q, void *element) {
    list_append(q->list, element);
}


void queue_dequeue(queue_t *q) {
    list_rm_head(q->list);
}


int queue_size(queue_t *q) {
    return list_size(q->list);
}

