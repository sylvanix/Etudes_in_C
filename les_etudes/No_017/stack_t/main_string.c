#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "stack.h"


void print_stack_string(stack_t *s);

int main(void)
{
    stack_t s;
    stack_new(&s, sizeof(char *), NULL);
    print_stack_string(&s);

    // test stack_append()
    unsigned long int i;
    const char *names[] = {"Alpha", "Beta", "Gamma", "Delta", "Eta"};
    char *name;
    for (i = 0; i < 5; i++) {
        name = strdup(names[i]);
        stack_push(&s, &name);
    }
    print_stack_string(&s);

    stack_pop(&s);
    print_stack_string(&s);

    free(name);
    stack_free(&s);

}


void print_stack_string(stack_t *stack) {
    size_t sz = list_size(stack->list);
    printf("sz: %lu\n", sz);
    char *name;
    for(unsigned long int i=0; i<sz; i++) {
        list_get_at(stack->list, i, &name);
        printf("%lu : %s\n", i, name);
    }
    printf("\n");
}
