#include <assert.h>
#include <stdlib.h>
#include "stack.h"


void stack_new(stack_t *s, int elementSize, freeFunction freeFn) {
    s->list = malloc(sizeof(list_t));
    list_new(s->list, elementSize, freeFn);
}


void stack_free(stack_t *s) {
    list_free(s->list);
    free(s->list);
}


void stack_push(stack_t *s, void *element) {
    list_prepend(s->list, element);
}


void stack_pop(stack_t *s) {
    // don't pop an empty stack!
    assert(stack_size(s) > 0);

    //list_head(s->list, element, true);
    list_rm_head(s->list);
}


int stack_size(stack_t *s) {
    return list_size(s->list);
}
