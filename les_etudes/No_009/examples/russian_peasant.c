
#include <stdio.h>


unsigned int russianPeasant(unsigned int a, unsigned int b);

int main()
{
    printf("%u\n", russianPeasant(18, 1));
    printf("%u\n", russianPeasant(20, 12));
    return 0;
} 


// A method to multiply two numbers using Russian Peasant method
unsigned int russianPeasant(unsigned int a, unsigned int b) {
    int res = 0;  // initialize result

    // While second number doesn't become 1
    while (b > 0)
    {
        // If second number becomes odd, add the first number to result
        if (b & 1)
            res = res + a;

        // Double the first number and halve the second number
        a = a << 1;
        b = b >> 1;
    }
    return res;
}


/*
Notes:
  1. If

Sources:
  https://www.geeksforgeeks.org/russian-peasant-multiply-two-numbers-using-bitwise-operators/
*/
