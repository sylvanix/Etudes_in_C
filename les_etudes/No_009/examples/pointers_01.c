#include <stdio.h>


void fun1(int *p); 
void fun2(int **p); 

int main()
{ 
    int r = 20;
    int *p = &r;
    printf("Pointer p, *p = %d\n", *p);
    fun1(p);
    printf("After passing pointer p to fun1, *p = %d\n", *p);
    fun2(&p);
    printf("After passing a pointer to pointer p to fun2, *p = %d\n", *p);

}


void fun1(int *p) {
    int q = 10;
    p = &q;
}


void fun2(int **pptr)
{
    static int q = 10;
    *pptr = &q;
}

/*
Notes:
  1. If we want to change a local pointer of one function inside another
     function, then we must pass pointer to the pointer.
  2. Static variables exist in memory even after functions return

Sources:
  https://www.geeksforgeeks.org/c-pointers-question-9/
*/
