# include <stdio.h>

int afunc(int x);

int main()
{   
    int val = 2;
    int sq = (*afunc)(val);
    printf("the squared integer: %d\n", sq);
}


int afunc(int x) {
    return x*2;
}

/*
Notes:
  For more advanced usage, see "callbacks"
*/
