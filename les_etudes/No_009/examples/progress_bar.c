#include <stdio.h>

const char *progress = "-\\|/";

int main()
{
    printf("Initializing array [-]");
    int i;
    for(i = 0; i < 10000000000; i++)
    {
        if(i % 1000 == 0)
        {
            // for the perplexed: ascii 8 is "backspace"
            printf("%c%c%c]",8,8,progress[(i/100) % 4]);
            fflush(stdout);
        }
        //a[i].p = random() % 100;
    }
    printf("\n");
}
