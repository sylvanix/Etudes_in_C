# include <stdio.h>
# include <stdlib.h>  /* exit() */

# define BUFSIZ (1000)

int main()
{
   char buffer[BUFSIZ];
   FILE *fptr;

   fptr = fopen("outfile.txt", "w");
   if(fptr == NULL) {
      printf("Error!");
      exit(1);
   }
   
   printf("Enter a sentence:\n");
   fgets(buffer, sizeof(buffer), stdin);

   fprintf(fptr,"%s", buffer);
   fclose(fptr);

   return 0;
}
