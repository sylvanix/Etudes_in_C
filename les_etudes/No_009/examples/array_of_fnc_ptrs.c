#include <stdio.h>

int sum(int a, int b);
int sub(int a, int b);
int mul(int a, int b);
int div(int a, int b);

int (*p[4]) (int x, int y); /* define the array of function pointers */

int main(void)
{
    int result;
    int i, j, op;
    
    p[0] = sum; /* address of sum() */
    p[1] = sub; /* address of subtract() */
    p[2] = mul; /* address of mul() */
    p[3] = div; /* address of div() */
    
    //To call one of those function pointers:
    // result = (*p[op]) (i, j); // op being the index of one of the four functions
    i = 6;
    j = 3;
    op = 1;
    result = (*p[op]) (i, j);
    printf("%d\n", result);

    return 0;
}


int sum(int a, int b) {
    return a + b;
}

int sub(int a, int b) {
    return a - b;
}

int mul(int a, int b) {
    return a * b;
}

int div(int a, int b) {
    return a / b;
}
