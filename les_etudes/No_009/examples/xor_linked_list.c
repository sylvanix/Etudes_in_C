#include <stdio.h>
#include <malloc.h>
#include <stdint.h>  // intptr_t

typedef struct xorll {
   int  value;
   struct xorll  *np;
}  xorll;


// traverse the list given either the head or the tail
void traverse( xorll *start )  // point to head or tail
{
   xorll *prev, *cur;

   cur = prev = start;
   while ( cur )
      {
      printf( "value = %d\n", cur->value );
      if ( cur->np == cur )
         // done
         break;
      if ( cur == prev )
         cur = cur->np;   // start of list
      else {
         xorll *save = cur;
         cur = (xorll*)((uintptr_t)prev ^ (uintptr_t)cur->np);
         prev = save;
         }
      }
}

// create a new node adding it to the given end and return it
xorll* newnode( xorll *prev, xorll *cur, int value )
{
   xorll *next;

   next = (xorll*)malloc( sizeof( xorll ));
   next->value = value;
   next->np = cur;  // end node points to previous one

   if ( cur == NULL )
      ; // very first node - we'll just return it
   else if ( prev == NULL ) {
      // this is the second node (they point at each other)
      cur->np = next;
      next->np = cur;
      }
   else {
      // do the xor magic
      cur->np = (xorll*)((uintptr_t)prev ^ (uintptr_t)next);
      }

   return next;
}



int main( int argc, char* argv[] )
{
   xorll *head, *tail;
   int   value = 1;

   // the first two nodes point at each other.  Weird param calls to
   // get the list started
   head = tail = newnode( NULL, NULL, value++ );
   tail = newnode( NULL, tail, value++ );

   // now add a couple to the end
   tail = newnode( tail->np, tail, value++ );
   tail = newnode( tail->np, tail, value++ );

   // this is cool - add a new head node
   head = newnode( head->np, head, 999 );


   printf( "Forwards:\n" );
   traverse( head );
   printf( "Backwards:\n" );
   traverse( tail );

}


/*

  Source: https://stackoverflow.com/questions/3531972/c-code-for-xor-linked-list

  This question was posted:
  "I have been trying to implement XOR linked list and its operations but I have not been able to
do it properly. Is it possible to implement it in C since XOR link list involves operations on
addresses? I would be very thankful if some actual working code is given. "


  Implementation by: Mark Wilkins

  Mark's comments:
  " That's an interesting idea that I have not seen before. With today's fairly abundant memory, it
seems like a lot of complexity for little gain (although not all platforms are flush with memory).
Edit While doing my real work, my mind kept drifting back to it, so I added the function to create
the new node and put it on the given end. Prettier now. It's rather cool that both the addnode and
traverse functions are symmetrical. Neither needs to know the direction. Just give it one end of
the list and they operate correctly. And based on Darron's comment (thanks), I changed the int to
intptr_t for portability. "
*/
