#include <stdio.h>
#include <stdlib.h>

/*
 * This is a simple C program to demonstrate the usage of callbacks
 * The callback function is in the same file as the calling code.
 * The callback function can later be put into external library like
 * e.g. a shared object to increase flexibility.
 *
 */

#include <stdio.h>
#include <string.h>

typedef struct _MyMsg {
    int appId;
    char msgbody[32];
} MyMsg;

void myfunc(MyMsg *msg)
{
    if (strlen(msg->msgbody) > 0 )
        printf("App Id = %d \nMsg = %s \n",msg->appId, msg->msgbody);
    else
        printf("App Id = %d \nMsg = No Msg\n",msg->appId);
}


// Prototype declaration
void (*callback)(MyMsg *);

int main(void)
{
    MyMsg msg1;
    msg1.appId = 100;
    strcpy(msg1.msgbody, "This is a test\n");

    /*
     * Assign the address of the function "myfunc" to the function
     * pointer "callback" (may be also written as "callback = &myfunc;")
     */
    callback = myfunc;

    /*
     * Call the function (may be also written as "(*callback)(&msg1);")
     */
    callback(&msg1);
}



/*
  Notes:
  Another advantage of callbacks is that the calling function can pass whatever 
  parameters it wishes to the called functions. This allows correct information hiding:
  the code that passes a callback to a calling function does not need to know the
  parameter values that will be passed to the function. If it only passed the return 
  value, then the parameters would need to be exposed publicly

  This information hiding means that callbacks can be used when communicating
  between processes or threads, or through serialised communications and tabular
  data.

  Source:
  https://en.wikipedia.org/wiki/Callback_(computer_programming)
*/
