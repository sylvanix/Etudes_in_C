#include <stdio.h>

void reverseArray(int arr[], int start, int end);

int main()
{
    int base = 2;
    int exponent = 1000;
	int ar[400];
	int sum = 0;
	ar[0]=1;
	for(int j=1; j<400; j++)
		ar[j] = 0;

	for(int i=1; i<(exponent+1); i++) {
        //printf("Iteration %d\n", i); 

		ar[0] = base * ar[0];
		for(int k=1; k<400; k++) {
			ar[k] = base * ar[k] + ar[k-1]/10;
        }

		for(int j=0; j<400; j++) {
            ar[j] = ar[j] % 10;
        }

        //// - Print elements at each iteration -
        //// Note that this array holds the decimal digits
        //// in reverse order.
		//for(int j=0; j<400; j++) {
        //    printf("%d ", ar[j]);
        //}
        //printf("\n");
	}

	//for(int i=0; i<400; i++)
	//    sum = sum + ar[i];
	//printf("sum: %d\n", sum);


    // The array hoding the integer digits is in reverse order and contains leading zeros.
    // Therefore remove the zeros, then reverse the array before printing the digits.
    int nb_zeros = 0;
    for (int i=0; i<400; i++) {
        if (ar[i] == 0)
            nb_zeros += 1;
        else
            nb_zeros = 0;
    }

    // trim the zeros
    printf("number of trailing zeros: %d\n", nb_zeros);
    //printf("%d\n", ar[400-(nb_zeros+1)]);
    //int temp[400-nb_zeros];
    int digits[400-nb_zeros];
    for (int i=0; i<400-nb_zeros; i++) {
        digits[i] = ar[i];
    }

    // reverse the order
    reverseArray(digits, 0, 400-nb_zeros-1);

    // print the digits
    for (int i=0; i<400-nb_zeros; i++) {
        printf("%d", digits[i]);
    }
    printf("\n");
}


// Function to reverse an array
void reverseArray(int arr[], int start, int end) 
{ 
    int temp; 
    while (start < end) { 
        temp = arr[start];    
        arr[start] = arr[end]; 
        arr[end] = temp; 
        start++; 
        end--; 
    }    
}
