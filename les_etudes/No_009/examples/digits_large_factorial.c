#include <stdio.h>

void reverseArray(int arr[], int start, int end);


int main()
{
    int number = 100; // calculate the factorial of this integer
    int digit;
    int carry;
    int temp;
    int ar[200];
	for(int j=0; j<200; j++)
		ar[j] = 0;
    ar[0] = 1;

	for(int i=2; i<(number + 1); i++) {
        carry = 0;
	    for(int j=0; j<200; j++) {
            temp = (ar[j] * i) + carry;
            digit = temp%10;
            carry = temp/10;
            ar[j] = digit;
            
        }
	}

    // The array hoding the integer digits is in reverse order and contains leading zeros.
    // Therefore remove the zeros, then reverse the array before printing the digits.
    int nb_zeros = 0;
    for (int i=0; i<200; i++) {
        if (ar[i] == 0)
            nb_zeros += 1;
        else
            nb_zeros = 0;
    }

    // trim the zeros
    //printf("number of trailing zeros: %d\n", nb_zeros);
    int digits[200-nb_zeros];
    for (int i=0; i<200-nb_zeros; i++) {
        digits[i] = ar[i];
    }

    // reverse the order
    reverseArray(digits, 0, 200-nb_zeros-1);

    // print the digits
    printf("The factorial of %d is:\n", number);
    for (int i=0; i<200-nb_zeros; i++) {
        printf("%d", digits[i]);
    }
    printf("\n");

}


// Function to reverse an array
void reverseArray(int arr[], int start, int end) 
{ 
    int temp; 
    while (start < end) 
    { 
        temp = arr[start];    
        arr[start] = arr[end]; 
        arr[end] = temp; 
        start++; 
        end--; 
    }    
}
