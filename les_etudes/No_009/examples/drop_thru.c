// https://cboard.cprogramming.com/c-programming/124152-using-fall-through-feature-switch-statement.html

/* The Twelve Days of Christmas lyrics, showing the "fall through" feature of the switch statement 
*/
 
#include <stdio.h>
 
int main(void) {
 
    int day;
    char numbers[][13]={"empty","first","second","third","fourth","fifth","sixth",
                        "seventh","eight","ninth","tenth","eleventh","twelfth"};
    printf("\n\n\n\t\t   Welcome to the Twelve Days of Christmas");
    
    for(day = 1; day < 13; day++) {
        printf("\n\n On the %s Day of Christmas, My True Love Gave to Me:\n", numbers[day]);
        
        switch(day) {
            case 12: printf("\n 12 Drummers Drumming");
            case 11: printf("\n 11 Pipers Piping");
            case 10: printf("\n 10 Lords a'Leaping");
            case 9: printf("\n  9 Ladies Dancing");
            case 8: printf("\n  8 Maids a'Milking");
            case 7: printf("\n  7 Swans a'Swimming");
            case 6: printf("\n  6 Geese a'Laying");
            case 5: printf("\n  5 Golden Rings");
            case 4: printf("\n  4 Calling Birds");
            case 3: printf("\n  3 French Hens");
            case 2: printf("\n  2 Turtle Doves");
            case 1: 
              if(day > 1) 
                printf("\n  And a Partridge in a Pear Tree");
              else
                printf("\n  A Partride in a Pear Tree");
        }
        if(day < 12) {
            printf("\n\n\t\t   press enter to see the next day's presents");
            getchar();
        }
    }
     
    printf("\n\n\t\t\t     press enter when ready");
    day = getchar();
    
    return 0;
}
