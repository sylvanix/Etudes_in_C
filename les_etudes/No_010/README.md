## Assorted Systems Programming for Linux examples    

### Index  
001 - **create\_child\_processes.c**  
	Create child processes with fork() and reporting status upon exit().
	
002 - **limit\_process\_resources.c**  
	Run a simple prime number generator under the with a CPU resource limit (in seconds).  
	


  
