#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include "list.h"


/* instantiate a list */
void list_new(list_t *list, int elementSize, freeFunction freeFn) {
  assert(elementSize > 0);

  list->logicalLength = 0;
  list->elementSize = elementSize;
  list->head = list->tail = NULL;
  list->freeFn = freeFn;
}


/* free all list nodes, and finally the list itself */
void list_free(list_t *list) {
    listNode_t *current;
    while (list->head != NULL) {
        current = list->head;
        list->head = current->next;
  
        if (list->freeFn) {
            list->freeFn(current->data);
        }
        free(current->data);
        free(current);
    }
}


/* prepend an element to the list, growing the list by 1 element */
void list_prepend(list_t *list, void *element) {
    listNode_t *node = malloc(sizeof(listNode_t));
    node->data = malloc(list->elementSize);
    memcpy(node->data, element, list->elementSize);
  
    node->next = list->head;
    list->head = node;
  
    // first node?
    if (!list->tail) {
        list->tail = list->head;
    }
    list->logicalLength += 1;
}


/* append an element to the list, growing the list by 1 element */
void list_append(list_t *list, void *element) {
    listNode_t *node = malloc(sizeof(listNode_t));
    node->data = malloc(list->elementSize);
    node->next = NULL;
    
    memcpy(node->data, element, list->elementSize);
    
    if (list->logicalLength == 0) {
        list->head = list->tail = node;
    } else {
        list->tail->next = node;
        list->tail = node;
    }
    list->logicalLength++;
}


/* Insert the value at the given index, growing the list by 1 element*/
void list_insert_at(list_t *list, int index, void *element) {
    assert( (index >= 0) && (index <= list->logicalLength) );
    if (index == 0)
        list_prepend(list, element);
    else if (index == list->logicalLength)
        list_append(list, element);
    else {
        listNode_t *node = malloc(sizeof(listNode_t));
        node->data = malloc(list->elementSize);
        memcpy(node->data, element, list->elementSize);

        listNode_t *current;
        current = list->head;
        listNode_t *oldNext;
        int count = 0;
        while ( (current != NULL) && (count <= index) ) {
            if (count == (index - 1)) {
                oldNext = current->next;
                current->next = node;
            }
            if (count == index) {
                current->next = oldNext;
            }
            current = current->next;
            count += 1;
        }
        list->logicalLength++;
    }

}


/* change the value of an existing element */
void list_set_at(list_t *list, int index, void *element) {
    assert( (index >= 0) && (index <= list->logicalLength-1) );
    listNode_t *current = list->head;
    int count = 0;
    while ( (current != NULL) && (count <= index) ) {
        if (count == index) {
            memcpy(current->data, element, list->elementSize);
        }
        count += 1;
        current = current->next;
    }    
}


/* get the value of the element at the given index */
void list_get_at(list_t *list, int index, void *target) {
    int count = 0;
    listNode_t * current;
    current = list->head;
    while ( (current != NULL) && (count <= index) ) {
        if (count == index) {
            void *source = current->data;
            memcpy(target, source, list->elementSize);
        }
        current = current->next;
        count += 1;
    }
}


/* remove head node */
void list_rm_head(list_t *list) {
    assert(list->head != NULL);
    listNode_t *node = list->head;
    list->head = node->next;
    list->logicalLength--;
    free(node->data);
    free(node);
}


/* remove tail node */
void list_rm_tail(list_t *list) {
    listNode_t *oldTail = list->tail;
    listNode_t *current;
    current = list->head;
    int count = 0;
    while (current != NULL) {
        if (count == (list->logicalLength - 2)) {
            current->next = NULL;
            list->tail = current;
        }
        current = current->next;
        count += 1;
    }
    free(oldTail->data);
    free(oldTail);
    list->logicalLength--;
}


/* remove the node at the given index */
void list_rm_at(list_t * list, int index) { 
    if (index == 0)
        list_rm_head(list);
    else if (index == ((list->logicalLength)-1))
        list_rm_tail(list);
    else {
        int count = 0;
        listNode_t * current;
        current = list->head;
        listNode_t * temp;
        while (count <= index) {
            if ( count == (index - 1) ) {
                temp = current;  
            }
            if (count == index) {
                temp->next = current->next;
                free(current->data);
                free(current);
                list->logicalLength--;
                current = temp;
            }
            current = current->next;
            count += 1;
        }
    }
}


/* return the list length */
int list_size(list_t *list) {
    return list->logicalLength;
}
