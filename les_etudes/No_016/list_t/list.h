#ifndef __LINKEDLIST_H
#define __LINKEDLIST_H

#include <stdbool.h>
#include "common.h"

typedef bool (*listIterator)(void *);

typedef struct listNode_ {
    void *data;
    struct listNode_ *next;
} listNode_t;

typedef struct {
    int logicalLength;
    int elementSize;
    listNode_t *head;
    listNode_t *tail;
    freeFunction freeFn;
} list_t;

void list_new(list_t *list, int elementSize, freeFunction freeFn);

void list_free(list_t *list);

void list_prepend(list_t *list, void *element);

void list_append(list_t *list, void *element);

void list_insert_at(list_t *list, int index, void *element);

void list_set_at(list_t *list, int index, void *element);

void list_get_at(list_t *list, int index, void *target);

void list_rm_head(list_t *list);

void list_rm_tail(list_t *list);

void list_rm_at(list_t *list, int index);

int list_size(list_t *list);

#endif
