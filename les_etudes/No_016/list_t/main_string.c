#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "list.h"

void print_list_string(list_t *list);

int main(void)
{
    unsigned long int i; // loop index

    // test list_new()
    list_t l;
    list_new(&l, sizeof(char *), NULL);
    print_list_string(&l);

    // test list_append()
    const char *names[] = {"Alpha", "Beta", "Gamma"};
    char *name;
    for (i = 0; i < 3; i++) {
        name = strdup(names[i]);
        list_append(&l, &name);
    }
    print_list_string(&l);

    list_free(&l);
    
}


void print_list_string(list_t *list) {
    size_t sz = list_size(list);
    printf("sz: %lu\n", sz);
    char *name;
    for(unsigned long int i=0; i<sz; i++) {
        list_get_at(list, i, &name);
        printf("%lu : %s\n", i, name);
    }
    printf("\n");
}
