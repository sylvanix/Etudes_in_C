/*
Sorts ascending, then searches in array for an instance of a particular value
*/

// NOTE: The bsearch() function returns a pointer to a matching member of the array,
//    or NULL if no match is found. If the array has multiple matching elements the
//    return value will be a pointer to one of those elements. Which particular
//    element is unspecified.
//
//COMPILE: gcc search.c -o search -g -Wall -O3 -std=gnu11

#include <stdlib.h>
#include <stdio.h>
#include <time.h>

int cmpfunc(const void * a, const void * b);


int main()
{
    /* Intialize random number generator */
    time_t t;
    srand((unsigned) time(&t));

    /* Create an array of N random values from the interval [a, b) */
    int N = 20;   // number of array elements
    int arr[N];   // an array to sort and search
    int a = 140;  // lower bound for values
    int b = 160;  // upper bound for values
    for (int ii=0; ii<N; ii++) {
        arr[ii] = rand()%(b-a) + a;
        printf("%d: %d\n", ii, arr[ii]);
    }

    /* Sort ascending */
    qsort(arr, N, sizeof(int), cmpfunc);
    printf("sorting...\n");
    for (int ii=0; ii<N; ii++) {
        printf("%d: %d\n", ii, arr[ii]);
    }

    /* Binary Search */
    int *item;     // a pointer to the element sought (returned by bsearch)
    int key = 150; // the value sought in the array
    item = (int*) bsearch(&key, arr, N, sizeof(int), cmpfunc);
    if( item != NULL ) {
        int idx = ( (int*)item - (int*)arr ) / 1;
        printf("Found %d at index %d\n", *item, idx);
    }
    else
        printf("The key value %d could not be found\n", key);
}


/* Compare function for qsort() and bsearch() */
int cmpfunc(const void * a, const void * b) {
   return ( *(int*)a - *(int*)b );
}
