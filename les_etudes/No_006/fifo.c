#include <stdio.h>

int main()
{
    unsigned int szLb = 6;
    float arr[szLb];
    float temp[szLb];

    // initialize arrays to zero
    for(unsigned int ii=0; ii<szLb-1; ii++) {
        arr[ii] = 0.0;
        temp[ii] = 0.0;
    }

    for(unsigned int iLoop=0; iLoop<100; iLoop++) {

        // implement a FIFO array container
        if(iLoop < szLb) {
            arr[iLoop] = iLoop;
        }
        else {
            for(unsigned int ii=0; ii<szLb-1; ii++) {
                temp[ii] = arr[ii+1];
            }
            temp[szLb-1] = iLoop;
            for(unsigned int ii=0; ii<szLb; ii++) {
                arr[ii] = temp[ii];
            }
        }

        for(unsigned int ii=0; ii<szLb; ii++) {
            printf("%2.1f ", arr[ii]);
        }
        printf("\n");
    }
 

}


