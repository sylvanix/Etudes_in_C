// ~$ gcc -Wall -I./include -L./source test_write.c -lsharedMem -o writing -lrt

/* Testing sharedMem.c */

#include "./include/sharedMem.h"

#define ROWS (3)
#define COLS (5)

int main() {

    // the name of the existing shared memory segment
    const char *name = "/testing";

    // LONG INT - define the array to write to the segment
    int len = ROWS*COLS;
    long int istart = 123456789011;
    long int arr[len];
    for (int i=0; i<len; i++) {
        arr[i] = i+istart;
    }

    // calculate the number of bytes in array
    long int nbytes = len*sizeof(arr[0]);

    // write to the shared memory
    shm_write(name, arr, nbytes);

}
