// ~$ gcc -Wall -I./include -L./source create_segment.c -lsharedMem -o create -lrt

/* Testing sharedMem.c */

#include "./include/sharedMem.h"

int main() {

    const char *name = "/testing";
    shm_create(name);
}
