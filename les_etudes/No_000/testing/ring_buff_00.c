/*
https://github.com/angrave/SystemProgramming/wiki/Synchronization,-Part-8:-Ring-Buffer-Example

 $ gcc -o rb ring_buff.c -lpthread
*/

#include<stdio.h>
#include <pthread.h>
#include <semaphore.h>
// N must be 2^i
#define N (8)

void *b[N];
int in = 0, out = 0;
pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;
sem_t countsem, spacesem;

void init()
{
  sem_init(&countsem, 0, 0);
  sem_init(&spacesem, 0, N);
}


void enqueue(void *value)
{
  // wait if there is no space left:
  sem_wait( &spacesem );

  pthread_mutex_lock(&lock);
  b[ (in++) & (N-1) ] = value;
  pthread_mutex_unlock(&lock);

  // increment the count of the number of items
  sem_post(&countsem);
}


void *dequeue()
{
  // Wait if there are no items in the buffer
  sem_wait(&countsem);

  pthread_mutex_lock(&lock);
  void *result = b[(out++) & (N-1)];
  pthread_mutex_unlock(&lock);

  // Increment the count of the number of spaces
  sem_post(&spacesem);

  return result;
}

void main()
{
  int v[] = {0,1,2,3,4,5,6,7};
  int* p = &v[0];
  init();
  printf("after    init - in: %d  out: %d \n", in, out);
  for (int ibuff=0; ibuff!=20; ibuff++)
  {
    enqueue(p);
    printf("after enqueue - in: %d  out: %d \n", in, out);
    dequeue();
    printf("after dequeue - in: %d  out: %d \n", in, out);
  }
}
