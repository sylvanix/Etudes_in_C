# Ring-Buffer
a ring buffer implementation written in C

[//]: # (Image References)
[image1]: ./images/running.png

To run the makefile to compile ring_buffer.c, at the shell prompt type  
`make`  
Then run the executable file with  
`./ring_buffer`  

![alt text][image1]  

#### Motivation  
I need to process video frames streaming from a camera. The Python image processing code needs access to the binary data frames but I need to use the camera vendor API to capture frames and take advantage of certain hardware features. Creating a Cython wrapper for the vendor library proved too difficult. This problem is similar to one solved on a project I worked on last year by Rich, a guy whose engineering abilities I respect. He used the vendor SDK to control the camera (different camera, that one was longwave infrared, this one is visible) and grab frames. He wrote the frames to a ring buffer ("circular buffer" if you like) he wrote in C++. Then I was able to grab the next frame in the buffer from my Python code. It was a beautiful solution and worked flawlessly.  

Having heard him speak of mutex and semaphores I was intrigued. Finding myself now in a similar situation, I was eager to implement a ring buffer myself, only I wanted to do it in C. Having insufficient experience with C, I consulted many sources, have seen many useful code snippets, and generated a great many segmentation faults whilst learning the concepts. If by chance someone with C programming experience chances upon this repository and feels like telling me how this should have been done, I would be grateful for the advice. Even though I will probably just ask Rich if I can use his field-tested ring-buffer, I don't regret the hours of reading and making stupid mistakes by abusing pointers and pointers to pointers, and by semaphore blocking in a function and not being able to return to main().  

#### Implementation  
The buffer size is set to 8, but may be changed by setting the global N_FRAMES to the desired value.
The user is given three options:  
`(e)nqueue, (d)equeue, (q)uit`  
Entering e for enqueue adds a Frame to the buffer if space remains. Entering d for dequeue removes a Frame from the buffer if at least one remains.  

The buffer stores up to N_FRAMES copies of a struct named Frame defined as:  
```
/* a struct to hold our camera frame */
typedef struct _frame {
  char* frameHeader;
  void *imgData;
} Frame;
```
The function initializeBuffer() is first called to create an N_FRAMES element array on the heap, and initialize each Frame element. An Frame's initialized *frameHeader* is ".................", and each ROWS x COLS element in *imgData* is set to 0.000000 (data type double). When the user enters either e or d, pthread_create() creates a thread to call either enqueue or dequeue. It was not really necessary to spawn new threads to call these functions, but early on I had thought that it would be, and so just left it in. As a stand-in for actual image data, I created a Frame named currentFrame which has *frameHeader* set to "data frame header", and *imgData* set to all 5.555555 (I wanted a number easily distinguishable from the zeros used for the initialization). After each enqueue or dequeue I print the header followed by the first and last value of the ROWS x COLS size data.   


Note: The program testing/pfunc.c was written prior to ring_buffer.c to help me better understand dynamic memory and pointers to pointers.  

#### Sources
I used several sources to learn the necessary concepts. I borrowed most heavily from reference 1 below; the pseudocode solution found at the end of the example was the beginning of my code. Reference 2 is a great resource on pthreads and contains several examples. Reference 3 is a complete example of the ideas found in 1, using pthreads and mutex.   

1. https://github.com/angrave/SystemProgramming/wiki/Synchronization,-Part-8:-Ring-Buffer-Example
2. https://computing.llnl.gov/tutorials/pthreads/
3. https://github.com/stackptr/operating-systems/blob/master/proj3/producer-consumer.c
