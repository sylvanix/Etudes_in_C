/*
A linked list of a struct containing a linked list

Inspired by the program shared by paxdiablo at:
https://stackoverflow.com/questions/27768334/a-linked-list-of-linked-lists-in-c

Improvements-to-make:
1. give each inventory item a count, so that, for example, a player may
   posses 2 water flasks, 3 cloaks, etc.
2. I/O. Initialize players and player inventory from a file on disk. After play, save the
   modified file to disk.
3. header files

To compile:
    gcc lloll.c -o lloll -g -Wall -O3 -std=gnu11
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct sInventory {
    char name[50];
    struct sInventory *next;
} tInventory;

typedef struct sPlayer {
    char name[50];
    tInventory *firstItem;
    struct sPlayer *next;
} tPlayer;


// function declarations ///////////////////////////////////////////////////////////////////

void *chkMalloc (size_t sz);
void addPlayer (tPlayer **first, char *name);
void popPlayer (tPlayer **currPlayer);
void addItem (tPlayer *first, char *name);
void popItem (tPlayer *first);
void dumpDetails (tPlayer *currPlayer);
void emptyPlayerList(tPlayer **first);


// TESTING... //////////////////////////////////////////////////////////////////////////////
int main (void)
{
    // Init the linked list of Player structs
    tPlayer *firstPlayer = NULL;

    // Add a player and the player's inventory linked list items
    addPlayer (&firstPlayer, "Anorak");
    addItem (firstPlayer, "bag of holding");
    addItem (firstPlayer, "staff of magic missile");
    addItem (firstPlayer, "lembas bread");

    // Add a player and the player's inventory linked list items
    addPlayer (&firstPlayer, "Gwyn");
    addItem (firstPlayer, "bow of frostbite");
    addItem (firstPlayer, "leather quiver");

    // Add a player with no items in inventory
    addPlayer (&firstPlayer, "Ampix");

    // Add a player and the player's inventory linked list items
    addPlayer (&firstPlayer, "Hardrada");
    addItem (firstPlayer, "bastard sword");
    addItem (firstPlayer, "water skin");

    // Print the players and their items
    dumpDetails (firstPlayer);

    // free the memory of the Player and Inventory lists
    emptyPlayerList(&firstPlayer);

    return 0;
}


// function definitions ////////////////////////////////////////////////////////////////////

void *chkMalloc (size_t sz) {
    void *mem = malloc (sz);
    // Just fail immediately on error.
    if (mem == NULL) {
        printf ("Out of memory! Exiting.\n");
        exit (1);
    }
    // Otherwise we know it worked.
    return mem;
}

void addPlayer (tPlayer **first, char *name) {
    // Insert new item at start.
    tPlayer *newest = chkMalloc (sizeof (*newest));
    strcpy (newest->name, name);
    newest->next = *first;
    *first = newest;
}

void popPlayer (tPlayer **first) {
    // first remove this player's items
    tInventory *currItem;
    currItem = (*first)->firstItem;
    while (currItem != NULL) {
        tInventory *psave;
        psave = currItem->next;
        free(currItem);
        currItem = psave;
    }
    // now remove this player
    tPlayer *psave;
    psave = (*first)->next;
    free (*first);
    *first = psave;
}

void addItem (tPlayer *first, char *name) {
    // Insert at start of list.
    tInventory *newest = chkMalloc (sizeof (*newest));
    strcpy (newest->name, name);
    newest->next = first->firstItem;
    first->firstItem = newest;
}

void dumpDetails (tPlayer *currPlayer) {

    // For every player.
    while (currPlayer != NULL) {
        printf ("\n%s's items:\n", currPlayer->name);

        // For every item that player has.
        tInventory *currItem = currPlayer->firstItem;
        if (currItem == NULL) {
            printf ("   <<nothing>>\n");
        } else {
            while (currItem != NULL) {
                printf ("   %s\n", currItem->name);
                currItem = currItem->next;
            }
        }
        currPlayer = currPlayer->next;
    }
}

void emptyPlayerList(tPlayer **first) {
    while (*first != NULL) {
        popPlayer(first);
    }
    printf("\nBye.\n");
}
